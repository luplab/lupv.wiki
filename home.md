# 32-bit software stack (DBL + Linux + Busybox)

This wiki page explains how to reproduce the 32-bit Linux+Busybox software stack which can be used with LupV.

## Precompiled (compressed) artifacts

- Dumb bootloader binary compressed image: [dbl_lupv_32.bin.gz](uploads/bd1501a9fe0422ba94e8e1da152e6fcd/dbl_lupv_32.bin.gz)
  - Note that this file is already part of LupV's code base, so technically you don't need it.
- Linux binary compressed image: [Image.gz](uploads/60c694b0248c6297c8037ae25fe64e6d/Image.gz)
- Root file system compressed image: [rootfs.img.gz](uploads/378444f370bfda756d02f5b5aac5c464/rootfs.img.gz)

## Recompile software stack manually

### RISC-V cross-toolchain

- Clone `https://github.com/riscv-collab/riscv-gnu-toolchain`
- Configure with `$ ./configure --prefix=/path/to/riscv32-unknown-linux-gnu --with-arch=rv32gc --with-abi=ilp32d`
- Compile with `$ make linux`
- The resulting toolchain will be installed in `</path/to/riscv32-unknown-linux-gnu>` (change the path to where you want)
- Add the path to the toolchain binaries to your `$PATH` variable: `$ export PATH=</path/to/riscv32-unknown-linux-gnu>/bin:${PATH}` (put this line into your `.bashrc` or `.zshrc` for the export to be persistent)

### DBL

- Clone `https://gitlab.com/luplab/riscv-dbl`
- Compile with `$ make CONF=config_lupv32.mk CROSS_COMPILE=riscv32-unknown-linux-gnu- -j`
- Bios image is `build/dbl.bin`

### Linux

- Clone `https://gitlab.com/luplab/lupio/linux`
- Switch to branch `lupio` (if not already there)
- Copy [config_lupv_072621](uploads/534ba6aaccae73bd3715a4cb62a2f46a/config_lupv_072621) as `.config`
- Compile with `$ make ARCH=riscv CROSS_COMPILE=riscv32-unknown-linux-gnu- -j`
- Kernel image is `arch/riscv/boot/Image`

### BusyBox

- Clone `https://git.busybox.net/busybox/`
- Copy [busybox_config_072121](uploads/2510cf352e5876dc58e7e03da19c1179/busybox_config_072121) as `.config`
- Compile with `$ make -j`
- BusyBox binary is `busybox`

#### Root filesystem

- Create a new directory hierarchy to populate the root FS
```console
$ mkdir -p rootfs/contents && cd rootfs/contents
$ mkdir -p bin etc dev lib proc sbin sys tmp usr usr/bin usr/lib usr/sbin
```
- Copy BusyBox binary into root FS, and create necessary symlinks
```console
$ cp </path/to/busybox>/busybox bin/busybox
$ ln -s bin/busybox sbin/init
$ ln -s bin/busybox init
``` 
- Create `inittab` file
```console
$ cat << EOF > etc/inittab
# Mount special filesystems
::sysinit:/bin/busybox mount -t proc proc /proc
::sysinit:/bin/busybox mount -t tmpfs tmpfs /tmp
# Remount root partition in RW
::sysinit:/bin/busybox mount -o remount,rw /
# Install all of the BusyBox applets
::sysinit:/bin/busybox --install -s
# Run shell directly
/dev/console::sysinit:-/bin/ash
EOF
```
- Create character device for the console
```console
$ sudo mknod dev/console c 5 1
```
- Make a binary image out of this directory hierarchy
```console
$ cd ..
$ qemu-img create rootfs.img 2M
$ echo ";" | sfdisk rootfs.img
$ sudo kpartx -a rootfs.img
$ sudo mkfs -t ext2 /dev/mapper/loop0p1
$ mkdir -p mountfs
$ sudo mount /dev/mapper/loop0p1 mountfs
$ sudo cp -a contents/* mountfs
$ sudo chown -R root:root mountfs
$ sudo umount mountfs
$ rm -rf mountfs
$ sudo kpartx -d rootfs.img
```

### Run LupV32 emulator

```console
$ lupv32 -b </path/to/dbl>/build/dbl.bin </path/to/linux>/arch/riscv/boot/Image </path/to/rootfs>/rootfs.img
```